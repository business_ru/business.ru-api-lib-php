<?php
/*
 * Библиотека работы с вебхуками Бизнес.Ру
 */

class Business_ru_webhook_lib
{
    private $app_id = '';
    private $secret = '';

    public function __construct($app_id, $secret)
    {
        $this->app_id = $app_id;
        $this->secret = $secret;
    }

    /**
     * Проверка подлинности уведомления
     * @return bool
     */
    public function authenticate()
    {
        $params = [];

        if(isset($this->$_REQUEST['app_id'])){
            $params['app_id'] = $this->$_REQUEST['app_id'];
            if($params['app_id'] != $this->app_id)
                return false;
        }

        if(isset($this->$_REQUEST['model']))
            $params['model'] = $this->$_REQUEST['model'];

        if(isset($this->$_REQUEST['action']))
            $params['action'] = $this->$_REQUEST['action'];

        if(isset($this->$_REQUEST['changes']))
            $params['changes'] = $this->$_REQUEST['changes'];

        if(isset($this->$_REQUEST['data']))
            $params['data'] = $this->$_REQUEST['data'];


        if(!isset($this->$_REQUEST['app_psw']))
            return false;

        if(MD5($this->secret.http_build_query($params)) != $this->$_REQUEST['app_psw'])
            return false;

        return true;
    }
}

?>